import json
import requests
from datetime import datetime

# root_url = 'https://app.ticketmaster.com/discovery/v2/'
# url = 'https://app.ticketmaster.com/discovery/v2/events.json?apikey=m4JXBVFlvEAb3MuwGgZPz90wZMqU2B8n'
us = 'https://app.ticketmaster.com/discovery/v2/events.json?countryCode=US&apikey=m4JXBVFlvEAb3MuwGgZPz90wZMqU2B8n'


class TicketMaster(object):
    def __init__(self):
        pass

    def search(self, stubhub_event):
        name = stubhub_event['name']
        date = stubhub_event['eventDateLocal'].rsplit('-', 1)[0] + 'Z'

        # d = datetime.strptime(date, "%Y-%M-%DT%H:%M:%S%Z")

        # YYYY-MM-DDTHH:mm:ssZ

        city = stubhub_event['venue']['city']
        lat = stubhub_event['venue']['latitude']
        lng = stubhub_event['venue']['longitude']

        #q_params = '&latlong={},{}&keyword={}&city={}&startDateTime={}'.format(
        q_params = '&keyword={}&city={}&startDateTime={}'.format(
             name, city, date
        )
        q_url = us + q_params

        print q_url

        res = requests.get(q_url)
        res_json = res.json()
        if res_json['page']['totalPages']>0:
            return res_json


    def q_search(self, name):

        q_params = '&keyword={}'.format(
             name,
        )
        q_url = us + q_params

        #print q_url

        res = requests.get(q_url)
        res_json = res.json()
        if res_json['page']['totalPages']>0:
            return res_json

    #GET /commerce/v2/events/0B00508C829A3875/offers.json?{apikey}
    def seats(self,event_ID):

        evt = 'https://app.ticketmaster.com/commerce/v2/events/{}/offers.json?apikey={}'.format(
            event_ID,
            'm4JXBVFlvEAb3MuwGgZPz90wZMqU2B8n'
        )
        res = requests.get(url=evt)
        return res.json()


if __name__ == '__main__':
    tm = TicketMaster()
    test_stubhub_event = '{"status":"Active","ticketInfo":{"totalPostings":242,"maxPriceWithCurrencyCode":{"currency":"USD","amount":6158.5},"maxListPrice":5130,"minPriceWithCurrencyCode":{"currency":"USD","amount":134.49},"currencyCode":"USD","popularity":0,"maxPrice":6158.5,"totalTickets":1072,"totalListings":242,"minListPriceWithCurrencyCode":{"currency":"USD","amount":109.99},"maxListPriceWithCurrencyCode":{"currency":"USD","amount":5130},"minListPrice":109.99,"minPrice":134.49},"description":"Paul McCartney Prudential Center Tickets - Buy and sell Paul McCartney Newark Tickets for September 12 at Prudential Center in Newark, NJ on StubHub!","locale":"en_GB","venue":{"city":"Newark","longitude":-74.170268,"name":"Prudential Center","url":"prudential-center-tickets","address1":"165 Mulberry Street","venueUrl":"prudential-center-tickets","webURI":"prudential-center-tickets/venue/91894/","postalCode":"07102","state":"NJ","seoURI":"prudential-center-tickets","country":"US","latitude":40.733747,"timezone":"EDT","jdkTimezone":"US/Eastern","id":91894,"venueConfigId":107387},"webURI":"paul-mccartney-tickets-paul-mccartney-newark-prudential-center-9-12-2017/event/102965692/","sourceId":"0","eventDateLocal":"2017-09-12T20:00:00-0400","defaultLocale":"en_US","seoURI":"paul-mccartney-tickets-paul-mccartney-newark-prudential-center-9-12-2017","originalName":"Paul McCartney Tickets","venueConfiguration":{"id":107387,"name":"End Stage - Dynamic"},"id":102965692,"name":"Paul McCartney"}'
    #tm.search(json.loads(test_stubhub_event))
    print json.dumps(tm.seats('1E0051969AEF0E99'))

