import json

from stubhub.tasks import StubHub
from ticketmaster.tasks import TicketMaster


# get from stubhub
stubhub = StubHub()
events_s = stubhub.events()

# search in ticketmaster
ticketmaster = TicketMaster()

for event in events_s:

    print 'searching ticket master for [{}]'.format(event['name'])
    event_t = ticketmaster.search(event)

    if event_t:
        print json.dumps(event)
        print ''
        print json.dumps(event_t)
        #break
        #pass # Save
    else:
        print 'Event Not Found on ticketmaster'
