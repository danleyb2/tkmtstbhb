# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.


class Event(models.Model):
    name_s = models.CharField(max_length=100)
    link_s = models.URLField()
    #min_price =