from django import forms

class SearchForm(forms.Form):

    event_name = forms.CharField(label='Event name', max_length=200)
