import json
import requests

# home = 'https://www.stubhub.com/'
us = 'https://api.stubhub.com/search/catalog/events/v3?minAvailableTickets=1&status=active |contingent&fieldList=ticketInfo,id,name,description,status,eventUrl,eventDateLocal,venue&country=US'


class StubHub(object):
    def __init__(self):
        self.headers = {
            # 'Accept':'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
            'Accept': 'application/json',
            'Authorization': 'Bearer dCriTPfqfVdb3oxsD_eWyUUtc38a',
            'User-Agent': 'okhttp/3.3.0 (j5lte; SM-J500F; samsung; 6.0.1; 3.2.1.1; 238.108.42.64; alive)',
            'Accept-Language': 'en-GB',
            'Connection': 'close',
            'Host': 'api.stubhub.com',
            'X-NewRelic-ID': 'VQYHVlBSABAGXVRWAAQD'

        }

    def events(self):
        res = requests.get(url=us, headers=self.headers)
        return res.json()['events']

    def q_search(self,name):
        q_e = 'https://api.stubhub.com/search/catalog/events/v3?name={}&minAvailableTickets=1&status=active |contingent&country=US'.format(name)
        res = requests.get(q_e, headers=self.headers)

        r= res.text
        print r

        return res.json()['events']

    def seats(self,event_ID):
        #9715707
        evt = 'https://api.stubhub.com/catalog/events/v2/{}?getZones=true'.format(event_ID)
        res = requests.get(url=evt, headers=self.headers)
        return res.json()


    def listings(self,event_id):
        # https://www.stubhub.com/shape
        #
        evt = 'https://api.stubhub.com/search/inventory/v2/listings/?eventId={}&sectionStats=true&zoneStats=true&start=0&allSectionZoneStats=true&eventLevelStats=true&quantitySummary=true&rows=20&sort=price+asc,value+desc&edgeControlEnabled=true&priceType=nonBundledPrice&valuePercentage=true&shstore=1'.format(event_id)
        res = requests.get(url=evt, headers=self.headers)
        return res.json()



def run():

    stubhub = StubHub()
    #print json.dumps(stubhub.events())

    print json.dumps(stubhub.q_search('colorado'))


if __name__ == '__main__':
    run()
